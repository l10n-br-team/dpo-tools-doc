# README

dpo-tools é um conjunto de shell scripts que podem auxiliar na tradução
de arquivos podebconf do Debian.

dpo-tools não dita nenhuma regra de como as traduções devem ser feitas,
ele apenas tenta refletir as convenções usadas na lista
debian-l10n-portuguese@lists.debian.org.

Os scripts são os seguintes:

* dpo-get-templates: faz o download de um arquivo templates.pot a partir do
repositório de templates podebconf.

* dpo-init-po: cria e inicializa um arquivo podebconf a partir de um arquivo
de templates.

* dpo-apply-templates: atualiza um arquivo podebconf com uma nova versão de
seu arquivo de templates.

* dpo-apply-compendium:  atualiza um arquivo podebconf com strings já
traduzidas, a partir de um arquivo compendium.

* dpo-check: verifica se o arquivo podebconf está bem formado.

* dpo-diff: apresenta informações no formato diff de forma colorida e paginada.

* dpo-make-patch: cria um arquivo de patch baseado em duas versões de um
arquivo podebconf.

* dpo-wrap: quebra as linhas de um arquivo podebconf na coluna 80.

## INSTALAÇÃO

Copie os scripts para algum diretório que fique em seu PATH.

	$ cp scripts/dpo-* ~/bin/
	ou
	$ sudo cp sripts/dpo-* /usr/local/bin/

## DEPENDÊNCIAS

O dpo-tools depende de outros programas para funcionar corretamente. Você deve
ter os seguintes programas principais instalados em seu computador:

* Utilitários do gettext: msgfmt, msgcat, msgmerge
* awk
* colordiff

Para instalá-los em um sistema Debian ou derivado:

	$ sudo apt install gettext gawk colordiff

Você vai querer também usar o programa podebconf-display-po. Para instalá-lo em
um sistema Debian ou derivado:

	$ sudo apt install po-debconf

## CONFIGURAÇÃO

* dpo-init-po.awk: ajuste as variáveis NOME e EMAIL. Se você usa o gawk, a
variável ANO deve ser ajustada no início de cada ano. Você pode
descomentar a linha que detecta automaticamente o ano, se o seu
interpretador awk tiver suporte à função strftime().

* dpo-init.po: edite a última linha do arquivo para refletir o caminho
completo do arquivo dpo-init.po.awk, por exemplo: ~/bin/dpo-init-po.awk

* dpo-make-patch: ajuste a variável ID com as iniciais do seu nome.

* dpo-apply-compendium: ajuste a variável COMPENDIUM para refletir o
caminho completo do arquivo de compendium, baixado de
http://i18n.debian.org/compendia/pt_BR/

## DESINSTALAÇÃO

Apenas remova os scripts.

## EXEMPLOS

Para iniciar uma nova tradução: a seguinte sequência de comandos faz o download
de um arquivo de templates podebconf, cria um arquivo podebconf baseado no
arquivo de templates baixado, inicializa o arquivo podebconf com informações
frequentemente usadas, atualiza o arquivo podebconf com strings do compendium,
edita o arquivo podebconf, quebra as linhas do arquivo podebconf na coluna 80,
verifica se o arquivo podebconf está bem formado e testa a exibição do arquivo
podebconf:

	$ dpo-get-templates pacote
	$ dpo-init-po pacote_version_templates.pot
	$ dpo-apply-compendium pacote_pt_BR.po
	$ vi pacote_pt_BR.po
	$ dpo-wrap pacote_pt_BR.po
	$ dpo-check pacote_pt_BR.po
	$ podebconf-display-po pacote_pt_BR.po
	envie o arquivo pacote_pt_BR.po para RFR

Para revisar uma tradução feita por outro tradutor e gerar um patch com
sugestões de alteração:

	substitua "eu" por suas iniciais nos comandos a seguir
	$ cp pacote_pt_BR.po pacote_pt_BR.eu.po
	$ vi pacote_pt_BR.eu.po
	$ dpo-wrap pacote_pt_BR.eu.po
	$ dpo-check pacote_pt_BR.eu.po
	$ podebconf-display-po pacote_pt_BR.eu.po
	$ dpo-make-patch pacote_pt_BR.po
	envie o arquivo de patch gerado, respondendo ao RFR

Para aplicar uma nova versão do arquivo de templates a um arquivo podebconf já
traduzido anteriormente:

	$ dpo-get-templates pacote_pt_BR.po
	$ dpo-apply-templates pacote_pt_BR.po

## CONTRIBUIÇÃO

Contribuições são bem-vindas, seja criando issus no repositório git ou na
forma de merge requests. Por favor, acesse:

https://salsa.debian.org/l10n-br-team/dpo-tools

## AUTORES

* Adriano Rafael Gomes <adrianorg@arg.eti.br>.
* Paulo Henrique de Lima Santana (phls) <paulo@phls.com.br>

## COPYRIGHT

	Copyright (C) 2008,2012 Adriano Rafael Gomes.
	Copyright (C) 2018       Paulo Henrique de Lima Santana (phls) <paulo@phls.com.br>.
	Licença GPLv3+: GNU GPL versão 3 ou posterior
	<http://gnu.org/licenses/gpl.html>.
	This is free software: you are free to change and redistribute it.
	There is NO WARRANTY, to the extent permitted by law.

Fonte:
https://www.arg.eti.br/blog/pages/software/dpo-tools/manual-do-usuario-do-dpo-tools.html
